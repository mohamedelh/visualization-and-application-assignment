import RPi.GPIO as GPIO
import time

class DoorSwitch:

    def __init__(self):
        GPIO.setmode(GPIO.BCM)
        self.isOpen = None
        self.oldIsOpen = None
        self.DOOR_SENSOR_PIN = 18
        GPIO.setup(self.DOOR_SENSOR_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def getDoorData(self):
        oldIsOpen = self.isOpen
        isOpen = GPIO.input(self.DOOR_SENSOR_PIN)
        if isOpen and (isOpen != oldIsOpen):
            return "open"
        elif isOpen != oldIsOpen:
            return "closed"


