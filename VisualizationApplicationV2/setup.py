import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

requires = [
    'aiohttp',
    'aiohttp-jinja2',
    'RPi.GPIO',
    'asyncio',
    'aiohttp_jwt',
    'python-socketio',
    'sqlalchemy_aio',
    'sqlalchemy',
    'aiohttp_security[session]',
    'aiohttp_cors',
    'pyyaml',
]

setup(
    name='VisualizationBackend',
    version='0.0',
    description='Backend for assignment',
    long_description='',
    classifiers=[],
    author='Mohamed el Hadiyen',
    author_email='Mohamed.el.hadiyen@hva.nl',
    url='',
    keywords='backend visualization',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
)

