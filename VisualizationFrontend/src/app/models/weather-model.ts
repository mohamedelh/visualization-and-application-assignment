import {ChartCustom} from './chartcustom-model'

export class Weather {

  temperatureData: any[] = [0];
  dateData: any[] = [0];
  chart: ChartCustom;

  constructor () {
      this.temperatureData = []
      this.dateData = []
  }

  createChart(date: any[], temp: any[], substract: any) {
    if (this.dateData[0] != null) {
      for (var i = 0; i < 5; i++) {
          this.dateData.pop();
          this.temperatureData.pop();
      }
    }
    for (var i = 0; i < 5; i++) {
        this.dateData.push(date[i]);
        this.temperatureData.push(temp[i] - substract);
    }
    this.chart = new ChartCustom('canvas', 'line', this.dateData, this.temperatureData)
  }


}
