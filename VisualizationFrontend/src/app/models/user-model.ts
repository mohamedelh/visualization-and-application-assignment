export class User {
    code: number;
    username: string;
    password: string;

    constructor() {
      this.code = null;
      this.username = null;
      this.password = null;
    }
}
