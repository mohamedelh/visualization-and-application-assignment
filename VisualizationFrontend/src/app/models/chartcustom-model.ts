import { Chart } from 'chart.js';

export class ChartCustom {

  chart: Chart;
  type: string;
  labels: any[];
  data: any[];

  update() {
    this.chart.update();
  }

  constructor(id: string, type: string, labels: any[], data: any[]) {
    this.labels = labels;
    this.data = data;
    this.type = type;
      this.chart = new Chart(id, {
        type: type,
        data: {
          labels: this.labels,
          datasets: [
            {
              data: this.data,
              borderColor: '#ffffff',
              pointBackgroundColor: "#ffffff",
              pointBorderColor: "#ffffff",
              pointHoverBackgroundColor: "#55bae7",
              pointHoverBorderColor: "#55bae7",
              fill: false
            }
          ]
        },
        options: {
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              display: true,
              ticks: {
                    fontColor: "white"
                }
            }],
            yAxes: [{
              ticks: {
                fontColor: "white",
                stepSize: 5,
                beginAtZero: true
              },
              display: true
            }]
          }
        }
      })
  }
  }
