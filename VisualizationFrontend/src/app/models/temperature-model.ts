import {ChartCustom} from './chartcustom-model'

export class Temperature {

  temperatureData: any[];
  dateData: any[];
  chart: ChartCustom;

  constructor () {
      this.temperatureData = []
      this.dateData = []
  }

  createChart() {
    this.chart = new ChartCustom('canvas1', 'line', this.dateData, this.temperatureData)
  }

  pushData(date: any, temp: any) {
  this.temperatureData.push(temp)
  this.dateData.push(date)
    if (this.temperatureData.length > 5) {
      this.temperatureData.shift()
      this.dateData.shift()
    }
  this.chart.chart.data.datasets[0].data = this.temperatureData;
  this.chart.chart.data.labels = this.dateData;
  this.chart.update()
  }

}
