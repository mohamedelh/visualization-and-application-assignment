import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../models/user-model';
import { UserService } from '../services/user.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User;

  constructor(
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.user = new User();
  }

  onLogin() {
    this.userService.login(this.user).subscribe(user => console.log(user));
    this.router.navigate(['/overview']);
  }

  onRegister() {
    this.router.navigate(['/registration']);
  }

}
