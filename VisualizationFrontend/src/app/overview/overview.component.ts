import { Component, OnInit, OnChanges } from '@angular/core';
import { WebSocketService } from '../services/web-socket.service';
import { WeatherService } from '../services/weather.service';
import { Chart } from 'chart.js';
import {ChartCustom} from '../models/chartcustom-model'
import {Temperature} from '../models/temperature-model'
import {Weather} from '../models/weather-model'
@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {

  checkDoor: boolean;
  checkTemp: boolean;
  checkWeather: boolean;

  temperature: Temperature = new Temperature();
  weather: Weather = new Weather();
  doorData: string;

  constructor(
    private WebSocketService: WebSocketService,
    private WeatherService: WeatherService) { }

  setDoor() {
    this.checkDoor = true;
    this.checkTemp = false;
    this.checkWeather = false;
  }

  setTemp() {
    this.checkDoor = false;
    this.checkTemp = true;
    this.checkWeather = false;

  }


  setWeather() {
    this.checkDoor = false;
    this.checkTemp = false;
    this.checkWeather = true;
    this.WeatherService.getWeatherData().subscribe(data => {
    let date = data['list'].map(data => data.dt_txt);
    let temp = data['list'].map(data => data.main.temp);
    this.weather.createChart(date, temp, this.WeatherService.kelvinSubstract);
    });
  }

  ngOnInit() {
    this.checkDoor = false;
    this.checkTemp = false;
    this.checkWeather = false;
    this.temperature.createChart()
    this.WebSocketService.listen('temperature').subscribe((data) => {
      let received = JSON.parse(data);
      console.log(data)
      this.temperature.pushData(received.time, received.temperature);

    })

  }



}
