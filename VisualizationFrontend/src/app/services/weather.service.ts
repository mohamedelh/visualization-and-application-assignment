import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  kelvinSubstract = 273.15;
  API_URL = "http://api.openweathermap.org/data/2.5/forecast?id=2759794&APPID=6e31a19a131838606d957d2c0c1d0960";

  constructor(private http: HttpClient) { }

  getWeatherData(): Observable<any> {
    return this.http.get(this.API_URL);
  }
}
