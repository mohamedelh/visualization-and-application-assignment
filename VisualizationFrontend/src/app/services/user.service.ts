import { Injectable } from '@angular/core';
import { User } from '../models/user-model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

var httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl =  'http://192.168.137.3:9932/user';

  constructor(private http: HttpClient) { }

  login(user: User): Observable<User> {
    return this.http.post<User>(this.baseUrl + "/login", {
                    'username': user.username,
                    'password': user.password });
  }

  register(user: User): Observable<any> {
    return this.http.post(this.baseUrl + "/register", {
                    'code': user.code,
                    'username': user.username,
                    'password': user.password }, {observe: 'response'});
  }

}
