import { TestBed } from '@angular/core/testing';

import { Weather.Service } from './weather.service';

describe('Weather.Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Weather.Service = TestBed.get(Weather.Service);
    expect(service).toBeTruthy();
  });
});
