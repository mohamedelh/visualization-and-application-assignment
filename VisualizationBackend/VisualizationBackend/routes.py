from views.overview import socketio_service

def simple_route(config, name, url, fn):
    """
    Function to simplify creating routes in pyramid
    Takes the pyramid configuration, name of the route, url, and view
    function.
    """
    config.add_route(name, url)
    config.add_view(fn, route_name=name,
            renderer="chatter2:templates/%s.mako" % name)

def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('user.login', 'user/login', request_method='POST')
    config.add_route('user.login-o', 'user/login', request_method='OPTIONS')
    config.add_route('user.register', 'user/register', request_method='POST')
    config.add_route('user.test', 'user/test', request_method='GET')
    config.add_route('overview.data', 'overview')
    simple_route(config, 'socket_io', 'socket.io/*remaining', socketio_service)
