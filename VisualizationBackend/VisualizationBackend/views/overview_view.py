from pyramid.view import view_defaults, view_config
from socketio import socketio_manage
from socketio.namespace import BaseNamespace
from socketio.mixins import RoomsMixin, BroadcastMixin

class Overview(BaseNamespace, BroadcastMixin):

   def recv_connect(self):
        self.emit("you_just_connected", {'bravo': 'kid'})
        self.spawn(self.send_data)

    def send_data(self):
        self.emit("data", "hello")

linkjes = {'/overviewdata': Overview}

@view_config(route_name='socket_io')
def socketio_service(request):
    """ The view that will launch the socketio listener """

    socketio_manage(request.environ, namespaces=linkjes, request=request)

    return {}
