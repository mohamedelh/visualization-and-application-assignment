from pyramid.view import view_defaults, view_config

@view_defaults(accept='application/json', renderer='json')
class UserView:

    def __init__(self, request):
        self.request = request

    @view_config(route_name='user.test')
    def test(self):
        print("invoked")

    @view_config(route_name='user.login')
    def login(self):
        print(self.request.json)
        #username = self.request.json['username']
        #password = self.request.json['password']
        return {
             "result": "ok",
             "token": self.request.create_jwt_token(1)
              }

    @view_config(route_name='user.login-o')
    def default_login(self):
        return {}

    @view_config(route_name='user.register')
    def register(self):
        code = self.request.json['code']
        username = self.request.json['username'] 
        password = self.request.json['password']
        return {"works":"true"}
