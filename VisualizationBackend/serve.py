from socketio.server import SocketIOServer
from pyramid.paster import get_app
from gevent import monkey; monkey.patch_all()

if __name__ == '__main__':

    app = get_app('development.ini')
    print("192.168.137.253:6543 Listening")

    SocketIOServer(('192.168.137.253', 6543), app,
        resource="socket.io", policy_server=True,
        policy_listener=('192.168.137.253', 10843)).serve_forever()
